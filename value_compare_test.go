package dexpr

import "testing"

func Test_valueCompare(t *testing.T) {
	type args struct {
		a interface{}
		b interface{}
		t valueType
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			name:    "'apple' (string) is less than 'pineapple' (string)",
			args:    args{"apple", "pineapple", valueTypeString},
			want:    -1,
			wantErr: false,
		},
		{
			name:    "'apple' (string) is equal to 'apple' (string)",
			args:    args{"apple", "apple", valueTypeString},
			want:    0,
			wantErr: false,
		},
		{
			name:    "'pen' (string) is greater than 'apple' (string)",
			args:    args{"pen", "apple", valueTypeString},
			want:    1,
			wantErr: false,
		},
		{
			name:    "should return error when 123 (integer) and 'apple' (string) are being compared",
			args:    args{float64(123), "apple", valueTypeString},
			want:    0,
			wantErr: true,
		},
		{
			name:    "100 (integer) is less than 123 (integer)",
			args:    args{float64(100), float64(123), valueTypeInteger},
			want:    -1,
			wantErr: false,
		},
		{
			name:    "100 (integer) is equal to 100 (integer)",
			args:    args{float64(100), float64(100), valueTypeInteger},
			want:    0,
			wantErr: false,
		},
		{
			name:    "123 (integer) is greater than 100 (integer)",
			args:    args{float64(123), float64(100), valueTypeInteger},
			want:    1,
			wantErr: false,
		},
		{
			name:    "100 (float) is less than 123 (float)",
			args:    args{float64(100), float64(123), valueTypeFloat},
			want:    -1,
			wantErr: false,
		},
		{
			name:    "100 (float) is equal to 100 (float)",
			args:    args{float64(100), float64(100), valueTypeFloat},
			want:    0,
			wantErr: false,
		},
		{
			name:    "123 (float) is greater than 100 (float)",
			args:    args{float64(123), float64(100), valueTypeFloat},
			want:    1,
			wantErr: false,
		},
		{
			name:    "returns error when second argument has wrong value type",
			args:    args{float64(123), 100, valueTypeInteger},
			want:    0,
			wantErr: true,
		},
		{
			name:    "returns error when second argument has wrong value type",
			args:    args{float64(123), 100, valueTypeFloat},
			want:    0,
			wantErr: true,
		},
		{
			name:    "returns error when second argument has wrong value type",
			args:    args{"apple", 100, valueTypeString},
			want:    0,
			wantErr: true,
		},
		{
			name:    "returns error when first argument has wrong value type",
			args:    args{123, 100, valueTypeBoolean},
			want:    0,
			wantErr: true,
		},
		{
			name:    "returns error when second argument has wrong value type",
			args:    args{true, 100, valueTypeBoolean},
			want:    0,
			wantErr: true,
		},
		{
			name:    "returns error value type is unknown",
			args:    args{"whatever", 12345, valueTypeUnknown},
			want:    0,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := valueCompare(tt.args.a, tt.args.b, tt.args.t)
			if (err != nil) != tt.wantErr {
				t.Errorf("valueCompare() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("valueCompare() = %v, want %v", got, tt.want)
			}
		})
	}
}
