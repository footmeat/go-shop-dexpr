package dexpr

import "fmt"

// OpNoOp ...
type OpNoOp struct {
	operand Expression
}

// Evaluate evaluates no operand.
func (op *OpNoOp) Evaluate(args ...interface{}) (bool, error) {
	if op.operand == nil {
		return false, fmt.Errorf("OpNoOp.Evaluate(): operand is nil")
	}
	return op.operand.Evaluate(args...)
}
