package dexpr

// OpNot ... ... ... ... ... ... ...
type OpNot struct {
	operand Expression
}

// Evaluate evaluates boolean NOT operator.
func (op *OpNot) Evaluate(args ...interface{}) (bool, error) {
	b, err := op.operand.Evaluate(args...)
	return !b, err
}
