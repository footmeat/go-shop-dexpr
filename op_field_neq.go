package dexpr

// OpFieldNeq ...
type OpFieldNeq struct {
	field string
	value interface{}
	as    valueType
}

// Evaluate ...
func (op *OpFieldNeq) Evaluate(args ...interface{}) (bool, error) {
	if len(args) == 0 {
		panic("no arguments passed to OpFieldNeq on evaluation")
	}
	flds := args[0].(map[string]interface{})
	if v, ok := flds[op.field]; ok {
		eq, err := equals(v, op.value, op.as)
		if err != nil {
			return false, err
		}
		return !eq, nil
	}
	return true, nil
}
