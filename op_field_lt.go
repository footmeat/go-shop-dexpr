package dexpr

// OpFieldLT ...
type OpFieldLT struct {
	field string
	value interface{}
	as    valueType
}

// Evaluate ...
func (op *OpFieldLT) Evaluate(args ...interface{}) (bool, error) {
	if len(args) == 0 {
		panic("no arguments passed to OpFieldLT on evaluation")
	}
	flds := args[0].(map[string]interface{})
	if v, ok := flds[op.field]; ok {
		lt, err := lessThan(v, op.value, op.as)
		if err != nil {
			return false, err
		}
		return lt, nil
	}
	return false, nil
}
