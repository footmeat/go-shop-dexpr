package dexpr

func buildExpression(v interface{}) Expression {
	switch v.(type) {
	case bool:
		return NewConstExpression(v.(bool))
	case map[string]interface{}:
		return buildExpressionInner(v.(map[string]interface{}))
	}
	panic("buildExpression(): argument is neither (bool) nor (map[string]interface{})")
}

func buildExpressionInner(v map[string]interface{}) Expression {
	var expr Expression
	for k, v := range v {
		// Note, the name 'v' gets overridden.
		var ops []Expression
		if k == "$and" || k == "$or" || k == "$nor" {
			ops = make([]Expression, 0)
			for _, t := range v.([]interface{}) {
				ops = append(ops, buildExpression(t))
			}
		}
		switch k {
		case "$and":
			return NewOpAnd(ops...)
		case "$or":
			return NewOpOr(ops...)
		case "$nor":
			return NewOpNor(ops...)
		case "$not":
			return NewOpNot(buildExpression(v))
		default:
			expr = buildFieldExpression(k, v.(map[string]interface{}))
		}
	}
	return expr
}

func buildFieldExpression(f string, v map[string]interface{}) Expression {
	var vt valueType
	if as, ok := v["$as"]; ok {
		switch as {
		case "integer":
			vt = valueTypeInteger
			break
		case "float":
			vt = valueTypeFloat
			break
		case "string":
			vt = valueTypeString
			break
		case "boolean":
			vt = valueTypeBoolean
			break
		default:
			vt = valueTypeUnknown
		}
	} else {
		panic("value type ('$as' property) is not specified")
	}
	if vt == valueTypeUnknown {
		panic("unknown value type")
	}
	if t, ok := v["$eq"]; ok {
		return &OpFieldEq{field: f, as: vt, value: t}
	}
	if t, ok := v["$neq"]; ok {
		return &OpFieldNeq{field: f, as: vt, value: t}
	}
	if t, ok := v["$gt"]; ok {
		return &OpFieldGT{field: f, as: vt, value: t}
	}
	if t, ok := v["$lt"]; ok {
		return &OpFieldLT{field: f, as: vt, value: t}
	}
	if t, ok := v["$in"]; ok {
		return &OpFieldIn{field: f, as: vt, values: t.([]interface{})}
	}
	panic("unknown field operator")
}
