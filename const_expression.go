package dexpr

// ConstExpression ...
type ConstExpression struct {
	value bool
}

// Evaluate ...
func (e *ConstExpression) Evaluate(args ...interface{}) (bool, error) {
	return e.value, nil
}
