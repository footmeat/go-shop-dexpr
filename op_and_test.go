package dexpr

import (
	"testing"
)

func TestOpAnd_Evaluate(t *testing.T) {
	type fields struct {
		operands []Expression
	}
	type args struct {
		args []interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{
			name:    "returns false when at least one operand is false",
			fields:  fields{operands: []Expression{&falseExpression{}, &trueExpression{}}},
			args:    args{},
			want:    false,
			wantErr: false,
		},
		{
			name:    "returns true when all operands are true",
			fields:  fields{operands: []Expression{&trueExpression{}, &trueExpression{}}},
			args:    args{},
			want:    true,
			wantErr: false,
		},
		{
			name:    "should return error when operand list is nil",
			fields:  fields{operands: nil},
			args:    args{},
			want:    false,
			wantErr: true,
		},
		{
			name:    "should return error when operand list is empty",
			fields:  fields{operands: make([]Expression, 0)},
			args:    args{},
			want:    false,
			wantErr: true,
		},
		{
			name:    "should return error, when operands evaluate with error",
			fields:  fields{operands: []Expression{&trueExpression{}, &errorExpression{}}},
			args:    args{},
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			op := &OpAnd{
				operands: tt.fields.operands,
			}
			got, err := op.Evaluate(tt.args.args...)
			if (err != nil) != tt.wantErr {
				t.Errorf("OpAnd.Evaluate() error %v, wantErr = %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("OpAnd.Evaluate() = %v, want %v", got, tt.want)
			}
		})
	}
}
