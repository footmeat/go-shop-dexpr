package dexpr

import "fmt"

// OpNor ...
type OpNor struct {
	operands []Expression
}

// Evaluate evaluates boolean NOR operand.
func (op *OpNor) Evaluate(args ...interface{}) (bool, error) {
	if len(op.operands) == 0 {
		return false, fmt.Errorf("operand list is empty")
	}
	for _, t := range op.operands {
		if b, err := t.Evaluate(args...); err == nil {
			if b {
				return false, nil
			}
		} else {
			return false, err
		}
	}
	return true, nil
}
