package dexpr

import "testing"

func TestOpFieldEq_Evaluate(t *testing.T) {
	type fields struct {
		field string
		value interface{}
		as    valueType
	}
	type args struct {
		args []interface{}
	}
	tArgs := args{
		args: []interface{}{
			map[string]interface{}{
				"width":   float64(80),
				"name":    "Caesar",
				"enabled": true,
			},
		},
	}
	tErrArgs := args{
		args: []interface{}{
			map[string]interface{}{
				"width": "80",
			},
		},
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "should return true when integer field value matches",
			fields: fields{
				field: "width",
				value: float64(80),
				as:    valueTypeInteger,
			},
			args:    tArgs,
			want:    true,
			wantErr: false,
		},
		{
			name: "should return false when integer field value doesn't match",
			fields: fields{
				field: "width",
				value: float64(75),
				as:    valueTypeInteger,
			},
			args:    tArgs,
			want:    false,
			wantErr: false,
		},
		{
			name: "should return true when string field value matches",
			fields: fields{
				field: "name",
				value: "Caesar",
				as:    valueTypeString,
			},
			args:    tArgs,
			want:    true,
			wantErr: false,
		},
		{
			name: "should return false when string field value doesn't match",
			fields: fields{
				field: "name",
				value: "Octavian",
				as:    valueTypeString,
			},
			args:    tArgs,
			want:    false,
			wantErr: false,
		},
		{
			name: "should return true when boolean field value matches",
			fields: fields{
				field: "enabled",
				value: true,
				as:    valueTypeBoolean,
			},
			args:    tArgs,
			want:    true,
			wantErr: false,
		},
		{
			name: "should return false when boolean field value doesn't match",
			fields: fields{
				field: "enabled",
				value: false,
				as:    valueTypeBoolean,
			},
			args:    tArgs,
			want:    false,
			wantErr: false,
		},
		{
			name: "should return error when field type doesn't match operand value type",
			fields: fields{
				field: "width",
				value: float64(80),
				as:    valueTypeInteger,
			},
			args:    tErrArgs,
			want:    false,
			wantErr: true,
		},
		{
			name: "should return false when field is not within arguments",
			fields: fields{
				field: "height",
				value: float64(46),
				as:    valueTypeInteger,
			},
			args:    tErrArgs,
			want:    false,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			op := &OpFieldEq{
				field: tt.fields.field,
				value: tt.fields.value,
				as:    tt.fields.as,
			}
			got, err := op.Evaluate(tt.args.args...)
			if (err != nil) != tt.wantErr {
				t.Errorf("OpFieldEq.Evaluate() error %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("OpFieldEq.Evaluate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFieldEq_Panic(t *testing.T) {
	type fields struct {
		field string
		value interface{}
		as    valueType
	}
	type args struct {
		args []interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "should panic when no arguments passed",
			fields: fields{
				field: "width",
				value: float64(80),
				as:    valueTypeInteger,
			},
			args: args{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var errMsg string
			defer func() {
				if r := recover(); r == nil {
					t.Errorf(errMsg)
				}
			}()
			op := &OpFieldEq{
				field: tt.fields.field,
				value: tt.fields.value,
				as:    tt.fields.as,
			}
			op.Evaluate(tt.args.args...)
		})
	}
}
