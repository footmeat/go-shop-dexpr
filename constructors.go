package dexpr

// NewConstExpression returns a new constant expression.
func NewConstExpression(value bool) *ConstExpression {
	return &ConstExpression{value: value}
}

// NewOpAnd returns a new AND operator.
func NewOpAnd(operands ...Expression) *OpAnd {
	return &OpAnd{operands: operands}
}

// NewOpOr returns a new OR operator.
func NewOpOr(operands ...Expression) *OpOr {
	return &OpOr{operands: operands}
}

// NewOpNor returns a new NOR operator.
func NewOpNor(operands ...Expression) *OpNor {
	return &OpNor{operands: operands}
}

// NewOpNot returns a new NOT operator.
func NewOpNot(operand Expression) *OpNot {
	return &OpNot{operand: operand}
}

// NewOpIntFieldEq returns a new integer field equality operator.
func NewOpIntFieldEq(field string, value float64) *OpFieldEq {
	return &OpFieldEq{field: field, value: value, as: valueTypeInteger}
}

// NewOpFloatFieldEq returns a new float field equality operator.
func NewOpFloatFieldEq(field string, value float64) *OpFieldEq {
	return &OpFieldEq{field: field, value: value, as: valueTypeFloat}
}

// NewOpStrFieldEq returns a new string field equality operator.
func NewOpStrFieldEq(field string, value string) *OpFieldEq {
	return &OpFieldEq{field: field, value: value, as: valueTypeString}
}

// NewOpBoolFieldEq returns a new boolean field equality operator.
func NewOpBoolFieldEq(field string, value bool) *OpFieldEq {
	return &OpFieldEq{field: field, value: value, as: valueTypeBoolean}
}

// NewOpIntFieldNeq returns a new integer field non-equality operator.
func NewOpIntFieldNeq(field string, value float64) *OpFieldNeq {
	return &OpFieldNeq{field: field, value: value, as: valueTypeInteger}
}

// NewOpFloatFieldNeq returns a new float field non-equality operator.
func NewOpFloatFieldNeq(field string, value float64) *OpFieldNeq {
	return &OpFieldNeq{field: field, value: value, as: valueTypeFloat}
}

// NewOpStrFieldNeq returns a new string field non-equality operator.
func NewOpStrFieldNeq(field string, value string) *OpFieldNeq {
	return &OpFieldNeq{field: field, value: value, as: valueTypeString}
}

// NewOpBoolFieldNeq returns a new boolean field non-equality operator.
func NewOpBoolFieldNeq(field string, value bool) *OpFieldNeq {
	return &OpFieldNeq{field: field, value: value, as: valueTypeBoolean}
}

// NewOpIntFieldGT returns a new integer field greater than operator.
func NewOpIntFieldGT(field string, value float64) *OpFieldGT {
	return &OpFieldGT{field: field, value: value, as: valueTypeInteger}
}

// NewOpFloatFieldGT returns a new float field greater than operator.
func NewOpFloatFieldGT(field string, value float64) *OpFieldGT {
	return &OpFieldGT{field: field, value: value, as: valueTypeFloat}
}

// NewOpStrFieldGT returns a new string field greater than operator.
func NewOpStrFieldGT(field string, value string) *OpFieldGT {
	return &OpFieldGT{field: field, value: value, as: valueTypeString}
}

// NewOpBoolFieldGT returns a new boolean field greater than operator.
func NewOpBoolFieldGT(field string, value bool) *OpFieldGT {
	return &OpFieldGT{field: field, value: value, as: valueTypeBoolean}
}

// NewOpIntFieldLT returns a new integer field less than operator.
func NewOpIntFieldLT(field string, value float64) *OpFieldLT {
	return &OpFieldLT{field: field, value: value, as: valueTypeInteger}
}

// NewOpFloatFieldLT returns a new float field less than operator.
func NewOpFloatFieldLT(field string, value float64) *OpFieldLT {
	return &OpFieldLT{field: field, value: value, as: valueTypeFloat}
}

// NewOpStrFieldLT returns a new string field less than operator.
func NewOpStrFieldLT(field string, value string) *OpFieldLT {
	return &OpFieldLT{field: field, value: value, as: valueTypeString}
}

// NewOpBoolFieldLT returns a new boolean field less than operator.
func NewOpBoolFieldLT(field string, value bool) *OpFieldLT {
	return &OpFieldLT{field: field, value: value, as: valueTypeBoolean}
}

// NewOpIntFieldIn returns a new integer field equality operator.
func NewOpIntFieldIn(field string, values ...interface{}) *OpFieldIn {
	return &OpFieldIn{field: field, values: values, as: valueTypeInteger}
}

// NewOpFloatFieldIn returns a new float field equality operator.
func NewOpFloatFieldIn(field string, values ...interface{}) *OpFieldIn {
	return &OpFieldIn{field: field, values: values, as: valueTypeFloat}
}

// NewOpStrFieldIn returns a new string field equality operator.
func NewOpStrFieldIn(field string, values ...interface{}) *OpFieldIn {
	return &OpFieldIn{field: field, values: values, as: valueTypeString}
}

// NewOpBoolFieldIn returns a new boolean field equality operator.
func NewOpBoolFieldIn(field string, values ...interface{}) *OpFieldIn {
	return &OpFieldIn{field: field, values: values, as: valueTypeBoolean}
}
