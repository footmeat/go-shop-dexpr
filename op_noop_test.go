package dexpr

import (
	"testing"
)

func TestOpNoOp_Evaluate(t *testing.T) {
	type fields struct {
		operand Expression
	}
	type args struct {
		args []interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{
			name:    "returns true when operand is true",
			fields:  fields{operand: &trueExpression{}},
			args:    args{},
			want:    true,
			wantErr: false,
		},
		{
			name:    "returns false when operand is false",
			fields:  fields{operand: &falseExpression{}},
			args:    args{},
			want:    false,
			wantErr: false,
		},
		{
			name:    "should return error when operand is nil",
			fields:  fields{operand: nil},
			args:    args{},
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			op := &OpNoOp{
				operand: tt.fields.operand,
			}
			got, err := op.Evaluate(tt.args.args...)
			if (err != nil) != tt.wantErr {
				t.Errorf("OpNoOp.Evaluate() error %v, wantErr = %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("OpNoOp.Evaluate() = %v, want %v", got, tt.want)
			}
		})
	}
}
