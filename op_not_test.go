package dexpr

import (
	"testing"
)

func TestOpNot_Evaluate(t *testing.T) {
	type fields struct {
		operand Expression
	}
	type args struct {
		args []interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{
			name:    "returns true when operand is false",
			fields:  fields{operand: &falseExpression{}},
			args:    args{},
			want:    true,
			wantErr: false,
		},
		{
			name:    "returns false when operand is true",
			fields:  fields{operand: &trueExpression{}},
			args:    args{},
			want:    false,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			op := &OpNot{
				operand: tt.fields.operand,
			}
			got, err := op.Evaluate(tt.args.args...)
			if (err != nil) != tt.wantErr {
				t.Errorf("OpNot.Evaluate() error %v, wantErr = %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("OpNot.Evaluate() = %v, want %v", got, tt.want)
			}
		})
	}
}
