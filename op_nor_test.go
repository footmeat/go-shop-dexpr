package dexpr

import (
	"testing"
)

func TestOpNor_Evaluate(t *testing.T) {
	type fields struct {
		operands []Expression
	}
	type args struct {
		args []interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{
			name:    "returns false when at least one operand is true",
			fields:  fields{operands: []Expression{&falseExpression{}, &trueExpression{}}},
			args:    args{},
			want:    false,
			wantErr: false,
		},
		{
			name:    "returns true when all operands are false",
			fields:  fields{operands: []Expression{&falseExpression{}, &falseExpression{}}},
			args:    args{},
			want:    true,
			wantErr: false,
		},
		{
			name:    "should return error when operand list is nil",
			fields:  fields{operands: nil},
			args:    args{},
			want:    false,
			wantErr: true,
		},
		{
			name:    "should return error when operand list is empty",
			fields:  fields{operands: make([]Expression, 0)},
			args:    args{},
			want:    false,
			wantErr: true,
		},
		{
			name: "should return error, when after number of falsy operands one evaluates with error",
			// All operands that precede error-producing operand should evaluate to false.
			// Otherwise, the operator will quit evaluating its operands and will return false.
			fields:  fields{operands: []Expression{&falseExpression{}, &falseExpression{}, &errorExpression{}}},
			args:    args{},
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			op := &OpNor{
				operands: tt.fields.operands,
			}
			got, err := op.Evaluate(tt.args.args...)
			if (err != nil) != tt.wantErr {
				t.Errorf("OpNor.Evaluate() error %v, wantErr = %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("OpNor.Evaluate() = %v, want %v", got, tt.want)
			}
		})
	}
}
