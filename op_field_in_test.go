package dexpr

import "testing"

func TestOpFieldIn_Evaluate(t *testing.T) {
	type fields struct {
		field  string
		values []interface{}
		as     valueType
	}
	type args struct {
		args []interface{}
	}
	tArgs := args{
		args: []interface{}{
			map[string]interface{}{
				"width": float64(100),
			},
		},
	}
	tErrArgs := args{
		args: []interface{}{
			map[string]interface{}{
				"width": "100",
			},
		},
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "returns TRUE when field value is in the value list",
			fields: fields{
				field:  "width",
				values: []interface{}{float64(80), float64(100), float64(120)},
				as:     valueTypeInteger,
			},
			args:    tArgs,
			want:    true,
			wantErr: false,
		},
		{
			name: "returns FALSE when field value is not in the value list",
			fields: fields{
				field:  "width",
				values: []interface{}{float64(80), float64(120)},
				as:     valueTypeInteger,
			},
			args:    tArgs,
			want:    false,
			wantErr: false,
		},
		{
			name: "should returns error when field type and type of value list do not match",
			fields: fields{
				field:  "width",
				values: []interface{}{float64(80), float64(100), float64(120)},
				as:     valueTypeInteger,
			},
			args:    tErrArgs,
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			op := &OpFieldIn{
				field:  tt.fields.field,
				values: tt.fields.values,
				as:     tt.fields.as,
			}
			got, err := op.Evaluate(tt.args.args...)
			if (err != nil) != tt.wantErr {
				t.Errorf("OpFieldIn.Evaluate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("OpFieldIn.Evaluate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFieldIn_Panic(t *testing.T) {
	type fields struct {
		field  string
		values []interface{}
		as     valueType
	}
	type args struct {
		args []interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "should panic when no arguments passed",
			fields: fields{
				field:  "width",
				values: []interface{}{float64(80), float64(100)},
				as:     valueTypeInteger,
			},
			args: args{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var errMsg string
			defer func() {
				if r := recover(); r == nil {
					t.Errorf(errMsg)
				}
			}()
			op := &OpFieldIn{
				field:  tt.fields.field,
				values: tt.fields.values,
				as:     tt.fields.as,
			}
			op.Evaluate(tt.args.args...)
		})
	}
}
