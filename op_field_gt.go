package dexpr

// OpFieldGT ...
type OpFieldGT struct {
	field string
	value interface{}
	as    valueType
}

// Evaluate ...
func (op *OpFieldGT) Evaluate(args ...interface{}) (bool, error) {
	if len(args) == 0 {
		panic("no arguments passed to OpFieldGT on evaluation")
	}
	flds := args[0].(map[string]interface{})
	if v, ok := flds[op.field]; ok {
		gt, err := greaterThan(v, op.value, op.as)
		if err != nil {
			return false, err
		}
		return gt, nil
	}
	return false, nil
}
