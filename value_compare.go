package dexpr

import "fmt"

func valueCompare(a, b interface{}, t valueType) (int, error) {
	if t == valueTypeInteger || t == valueTypeFloat {
		// Numeric values
		switch a.(type) {
		case float64:
			switch b.(type) {
			case float64:
				if a.(float64) < b.(float64) {
					return -1, nil
				} else if a.(float64) > b.(float64) {
					return 1, nil
				}
				return 0, nil
			default:
				return 0, fmt.Errorf("valueCompare(): comparing %T, float64 expected", b)
			}
		default:
			return 0, fmt.Errorf("valueCompare(): comparing %T, float64 expected", a)
		}
	} else if t == valueTypeString {
		// String values
		switch a.(type) {
		case string:
			switch b.(type) {
			case string:
				if a.(string) < b.(string) {
					return -1, nil
				} else if a.(string) > b.(string) {
					return 1, nil
				}
				return 0, nil
			default:
				return 0, fmt.Errorf("valueCompare(): comparing %T, string expected", b)
			}
		default:
			return 0, fmt.Errorf("valueCompare(): comparing %T, string expected", a)
		}
	} else if t == valueTypeBoolean {
		// Boolean values
		switch a.(type) {
		case bool:
			switch b.(type) {
			case bool:
				if !a.(bool) && b.(bool) {
					return -1, nil
				} else if a.(bool) && !b.(bool) {
					return 1, nil
				}
				return 0, nil
			default:
				return 0, fmt.Errorf("valueCompare(): comparing %T, boolean expected", b)
			}
		default:
			return 0, fmt.Errorf("valueCompare(): comparing %T, boolean expected", a)
		}
	}
	return 0, fmt.Errorf("valueCompare(): comparing %T and %T", a, b)
}

func lessThan(a, b interface{}, t valueType) (bool, error) {
	ord, err := valueCompare(a, b, t)
	if err != nil {
		return false, err
	}
	return ord == -1, nil
}

func greaterThan(a, b interface{}, t valueType) (bool, error) {
	ord, err := valueCompare(a, b, t)
	if err != nil {
		return false, err
	}
	return ord == 1, nil
}

func equals(a, b interface{}, t valueType) (bool, error) {
	ord, err := valueCompare(a, b, t)
	if err != nil {
		return false, err
	}
	return ord == 0, nil
}
