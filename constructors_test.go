package dexpr

import (
	"reflect"
	"testing"
)

func TestNewConstExpression(t *testing.T) {
	type args struct {
		value bool
	}
	tests := []struct {
		name string
		args args
		want *ConstExpression
	}{
		{
			name: "returns truthy constant expression",
			args: args{value: true},
			want: &ConstExpression{value: true},
		},
		{
			name: "returns falsy constant expression",
			args: args{value: false},
			want: &ConstExpression{value: false},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewConstExpression(tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewConstExpression() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpAnd(t *testing.T) {
	type args struct {
		operands []Expression
	}
	tArgs := args{
		operands: []Expression{
			&trueExpression{},
			&falseExpression{},
		},
	}
	tests := []struct {
		name string
		args args
		want *OpAnd
	}{
		{
			name: "returns a correctly constructed AND operand",
			args: tArgs,
			want: &OpAnd{
				operands: []Expression{
					&trueExpression{},
					&falseExpression{},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpAnd(tt.args.operands...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpAnd() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpOr(t *testing.T) {
	type args struct {
		operands []Expression
	}
	tArgs := args{
		operands: []Expression{
			&trueExpression{},
			&falseExpression{},
		},
	}
	tests := []struct {
		name string
		args args
		want *OpOr
	}{
		{
			name: "returns a correctly constructed OR operand",
			args: tArgs,
			want: &OpOr{
				operands: []Expression{
					&trueExpression{},
					&falseExpression{},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpOr(tt.args.operands...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpOr() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpNor(t *testing.T) {
	type args struct {
		operands []Expression
	}
	tArgs := args{
		operands: []Expression{
			&trueExpression{},
			&falseExpression{},
		},
	}
	tests := []struct {
		name string
		args args
		want *OpNor
	}{
		{
			name: "returns a correctly constructed NOR operand",
			args: tArgs,
			want: &OpNor{
				operands: []Expression{
					&trueExpression{},
					&falseExpression{},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpNor(tt.args.operands...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpNor() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpNot(t *testing.T) {
	type args struct {
		operand Expression
	}
	tArgs := args{
		operand: &trueExpression{},
	}
	tests := []struct {
		name string
		args args
		want *OpNot
	}{
		{
			name: "returns a correctly constructed NOT operand",
			args: tArgs,
			want: &OpNot{
				operand: &trueExpression{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpNot(tt.args.operand); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpNot() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpIntFieldEq(t *testing.T) {
	type args struct {
		field string
		value float64
	}
	tests := []struct {
		name string
		args args
		want *OpFieldEq
	}{
		{
			name: "returns integer field equality operand",
			args: args{
				field: "width",
				value: float64(100),
			},
			want: &OpFieldEq{
				field: "width",
				value: float64(100),
				as:    valueTypeInteger,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpIntFieldEq(tt.args.field, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpIntFieldEq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpFloatFieldEq(t *testing.T) {
	type args struct {
		field string
		value float64
	}
	tests := []struct {
		name string
		args args
		want *OpFieldEq
	}{
		{
			name: "returns float field equality operand",
			args: args{
				field: "width",
				value: float64(100),
			},
			want: &OpFieldEq{
				field: "width",
				value: float64(100),
				as:    valueTypeFloat,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpFloatFieldEq(tt.args.field, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpFloatFieldEq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpStrFieldEq(t *testing.T) {
	type args struct {
		field string
		value string
	}
	tests := []struct {
		name string
		args args
		want *OpFieldEq
	}{
		{
			name: "returns string field equality operand",
			args: args{
				field: "name",
				value: "Caesar",
			},
			want: &OpFieldEq{
				field: "name",
				value: "Caesar",
				as:    valueTypeString,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpStrFieldEq(tt.args.field, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpStrFieldEq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpBoolFieldEq(t *testing.T) {
	type args struct {
		field string
		value bool
	}
	tests := []struct {
		name string
		args args
		want *OpFieldEq
	}{
		{
			name: "returns boolean field equality operand",
			args: args{
				field: "enabled",
				value: true,
			},
			want: &OpFieldEq{
				field: "enabled",
				value: true,
				as:    valueTypeBoolean,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpBoolFieldEq(tt.args.field, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpBoolFieldEq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpIntFieldNeq(t *testing.T) {
	type args struct {
		field string
		value float64
	}
	tests := []struct {
		name string
		args args
		want *OpFieldNeq
	}{
		{
			name: "returns integer field non-equality operand",
			args: args{
				field: "width",
				value: float64(100),
			},
			want: &OpFieldNeq{
				field: "width",
				value: float64(100),
				as:    valueTypeInteger,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpIntFieldNeq(tt.args.field, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpIntFieldNeq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpFloatFieldNeq(t *testing.T) {
	type args struct {
		field string
		value float64
	}
	tests := []struct {
		name string
		args args
		want *OpFieldNeq
	}{
		{
			name: "returns float field non-equality operand",
			args: args{
				field: "width",
				value: float64(100),
			},
			want: &OpFieldNeq{
				field: "width",
				value: float64(100),
				as:    valueTypeFloat,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpFloatFieldNeq(tt.args.field, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpFloatFieldNeq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpStrFieldNeq(t *testing.T) {
	type args struct {
		field string
		value string
	}
	tests := []struct {
		name string
		args args
		want *OpFieldNeq
	}{
		{
			name: "returns string field non-equality operand",
			args: args{
				field: "name",
				value: "Caesar",
			},
			want: &OpFieldNeq{
				field: "name",
				value: "Caesar",
				as:    valueTypeString,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpStrFieldNeq(tt.args.field, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpStrFieldNeq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpBoolFieldNeq(t *testing.T) {
	type args struct {
		field string
		value bool
	}
	tests := []struct {
		name string
		args args
		want *OpFieldNeq
	}{
		{
			name: "returns boolean field non-equality operand",
			args: args{
				field: "enabled",
				value: true,
			},
			want: &OpFieldNeq{
				field: "enabled",
				value: true,
				as:    valueTypeBoolean,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpBoolFieldNeq(tt.args.field, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpBoolFieldNeq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpIntFieldGT(t *testing.T) {
	type args struct {
		field string
		value float64
	}
	tests := []struct {
		name string
		args args
		want *OpFieldGT
	}{
		{
			name: "returns integer field greater than operand",
			args: args{
				field: "width",
				value: float64(100),
			},
			want: &OpFieldGT{
				field: "width",
				value: float64(100),
				as:    valueTypeInteger,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpIntFieldGT(tt.args.field, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpIntFieldGT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpFloatFieldGT(t *testing.T) {
	type args struct {
		field string
		value float64
	}
	tests := []struct {
		name string
		args args
		want *OpFieldGT
	}{
		{
			name: "returns float field greater than operand",
			args: args{
				field: "weight",
				value: float64(400.25),
			},
			want: &OpFieldGT{
				field: "weight",
				value: float64(400.25),
				as:    valueTypeFloat,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpFloatFieldGT(tt.args.field, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpFloatFieldGT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpStrFieldGT(t *testing.T) {
	type args struct {
		field string
		value string
	}
	tests := []struct {
		name string
		args args
		want *OpFieldGT
	}{
		{
			name: "returns string field greater than operand",
			args: args{
				field: "name",
				value: "Caesar",
			},
			want: &OpFieldGT{
				field: "name",
				value: "Caesar",
				as:    valueTypeString,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpStrFieldGT(tt.args.field, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpStrFieldGT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpBoolFieldGT(t *testing.T) {
	type args struct {
		field string
		value bool
	}
	tests := []struct {
		name string
		args args
		want *OpFieldGT
	}{
		{
			name: "returns boolean field greater than operand",
			args: args{
				field: "enabled",
				value: true,
			},
			want: &OpFieldGT{
				field: "enabled",
				value: true,
				as:    valueTypeBoolean,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpBoolFieldGT(tt.args.field, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpBoolFieldGT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpIntFieldLT(t *testing.T) {
	type args struct {
		field string
		value float64
	}
	tests := []struct {
		name string
		args args
		want *OpFieldLT
	}{
		{
			name: "returns integer field less than operand",
			args: args{
				field: "width",
				value: float64(100),
			},
			want: &OpFieldLT{
				field: "width",
				value: float64(100),
				as:    valueTypeInteger,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpIntFieldLT(tt.args.field, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpIntFieldLT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpFloatFieldLT(t *testing.T) {
	type args struct {
		field string
		value float64
	}
	tests := []struct {
		name string
		args args
		want *OpFieldLT
	}{
		{
			name: "returns float field less than operand",
			args: args{
				field: "weight",
				value: float64(400.25),
			},
			want: &OpFieldLT{
				field: "weight",
				value: float64(400.25),
				as:    valueTypeFloat,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpFloatFieldLT(tt.args.field, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpFloatFieldLT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpStrFieldLT(t *testing.T) {
	type args struct {
		field string
		value string
	}
	tests := []struct {
		name string
		args args
		want *OpFieldLT
	}{
		{
			name: "returns string field less than operand",
			args: args{
				field: "name",
				value: "Caesar",
			},
			want: &OpFieldLT{
				field: "name",
				value: "Caesar",
				as:    valueTypeString,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpStrFieldLT(tt.args.field, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpStrFieldLT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpBoolFieldLT(t *testing.T) {
	type args struct {
		field string
		value bool
	}
	tests := []struct {
		name string
		args args
		want *OpFieldLT
	}{
		{
			name: "returns boolean field less than operand",
			args: args{
				field: "enabled",
				value: true,
			},
			want: &OpFieldLT{
				field: "enabled",
				value: true,
				as:    valueTypeBoolean,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOpBoolFieldLT(tt.args.field, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOpBoolFieldLT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOpIntFieldIn(t *testing.T) {
	type args struct {
		field  string
		values []interface{}
	}
	tests := []struct {
		name string
		args args
		want *OpFieldIn
	}{
		{
			name: "returns integer field value-in operand",
			args: args{
				field:  "width",
				values: []interface{}{float64(100), float64(120)},
			},
			want: &OpFieldIn{
				field:  "width",
				values: []interface{}{float64(120), float64(100)},
				as:     valueTypeInteger,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewOpIntFieldIn(tt.args.field, tt.args.values...)
			m := make(map[float64]bool)
			for _, v := range tt.args.values {
				m[v.(float64)] = true
			}
			if len(m) != len(tt.want.values) {
				t.Errorf("NewOpIntFieldIn(): got the value set %v, want %v", got.values, tt.want.values)
			}
			for _, v := range tt.want.values {
				if _, ok := m[v.(float64)]; !ok {
					t.Errorf("NewOpIntFieldIn(): got the value set %v, want %v", got.values, tt.want.values)
				}
			}
			if got.field != tt.want.field {
				t.Errorf("NewOpIntFieldIn(): got field %v, want %v", got.field, tt.want.field)
			}
			if got.as != tt.want.as {
				t.Errorf("NewOpIntFieldIn(): got 'as' %v, want %v", got.as, tt.want.as)
			}
		})
	}
}

func TestNewOpFloatFieldIn(t *testing.T) {
	type args struct {
		field  string
		values []interface{}
	}
	tests := []struct {
		name string
		args args
		want *OpFieldIn
	}{
		{
			name: "returns float field value-in operand",
			args: args{
				field:  "weight",
				values: []interface{}{float64(100), float64(120)},
			},
			want: &OpFieldIn{
				field:  "weight",
				values: []interface{}{float64(120), float64(100)},
				as:     valueTypeFloat,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewOpFloatFieldIn(tt.args.field, tt.args.values...)
			m := make(map[float64]bool)
			for _, v := range tt.args.values {
				m[v.(float64)] = true
			}
			if len(m) != len(tt.want.values) {
				t.Errorf("NewOpFloatFieldIn(): got the value set %v, want %v", got.values, tt.want.values)
			}
			for _, v := range tt.want.values {
				if _, ok := m[v.(float64)]; !ok {
					t.Errorf("NewOpFloatFieldIn(): got the value set %v, want %v", got.values, tt.want.values)
				}
			}
			if got.field != tt.want.field {
				t.Errorf("NewOpFloatFieldIn(): got field %v, want %v", got.field, tt.want.field)
			}
			if got.as != tt.want.as {
				t.Errorf("NewOpFloatFieldIn(): got 'as' %v, want %v", got.as, tt.want.as)
			}
		})
	}
}

func TestNewOpStrFieldIn(t *testing.T) {
	type args struct {
		field  string
		values []interface{}
	}
	tests := []struct {
		name string
		args args
		want *OpFieldIn
	}{
		{
			name: "returns string field value-in operand",
			args: args{
				field:  "type",
				values: []interface{}{"sofa", "chair", "bed"},
			},
			want: &OpFieldIn{
				field:  "type",
				values: []interface{}{"chair", "sofa", "bed"},
				as:     valueTypeString,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewOpStrFieldIn(tt.args.field, tt.args.values...)
			m := make(map[string]bool)
			for _, v := range tt.args.values {
				m[v.(string)] = true
			}
			if len(m) != len(tt.want.values) {
				t.Errorf("NewOpStrFieldIn(): got the value set %v, want %v", got.values, tt.want.values)
			}
			for _, v := range tt.want.values {
				if _, ok := m[v.(string)]; !ok {
					t.Errorf("NewOpStrFieldIn(): got the value set %v, want %v", got.values, tt.want.values)
				}
			}
			if got.field != tt.want.field {
				t.Errorf("NewOpStrFieldIn(): got field %v, want %v", got.field, tt.want.field)
			}
			if got.as != tt.want.as {
				t.Errorf("NewOpStrFieldIn(): got 'as' %v, want %v", got.as, tt.want.as)
			}
		})
	}
}

func TestNewOpBoolFieldIn(t *testing.T) {
	type args struct {
		field  string
		values []interface{}
	}
	tests := []struct {
		name string
		args args
		want *OpFieldIn
	}{
		{
			name: "returns boolean field value-in operand",
			args: args{
				field:  "chosen",
				values: []interface{}{false, true},
			},
			want: &OpFieldIn{
				field:  "chosen",
				values: []interface{}{false, true},
				as:     valueTypeBoolean,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewOpBoolFieldIn(tt.args.field, tt.args.values...)
			m := make(map[bool]bool)
			for _, v := range tt.args.values {
				m[v.(bool)] = true
			}
			if len(m) != len(tt.want.values) {
				t.Errorf("NewOpBoolFieldIn(): got the value set %v, want %v", got.values, tt.want.values)
			}
			for _, v := range tt.want.values {
				if _, ok := m[v.(bool)]; !ok {
					t.Errorf("NewOpBoolFieldIn(): got the value set %v, want %v", got.values, tt.want.values)
				}
			}
			if got.field != tt.want.field {
				t.Errorf("NewOpBoolFieldIn(): got field %v, want %v", got.field, tt.want.field)
			}
			if got.as != tt.want.as {
				t.Errorf("NewOpBoolFieldIn(): got 'as' %v, want %v", got.as, tt.want.as)
			}
		})
	}
}
