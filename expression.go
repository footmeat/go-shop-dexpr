package dexpr

// Expression can be evaluated.
type Expression interface {
	Evaluate(...interface{}) (bool, error)
}

type trueExpression struct{}

func (e *trueExpression) Evaluate(args ...interface{}) (bool, error) {
	return true, nil
}

type falseExpression struct{}

func (e *falseExpression) Evaluate(args ...interface{}) (bool, error) {
	return false, nil
}

type valueType int

const (
	valueTypeUnknown valueType = 0
	valueTypeInteger valueType = 1
	valueTypeFloat   valueType = 2
	valueTypeString  valueType = 3
	valueTypeBoolean valueType = 4
)
