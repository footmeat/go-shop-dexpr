package dexpr

// OpFieldIn ...
type OpFieldIn struct {
	field  string
	values []interface{}
	as     valueType
}

// Evaluate ...
func (op *OpFieldIn) Evaluate(args ...interface{}) (bool, error) {
	if len(args) == 0 {
		panic("no arguments passed to OpFieldIn on evaluation")
	}
	flds := args[0].(map[string]interface{})
	if v, ok := flds[op.field]; ok {
		for _, ov := range op.values {
			if eq, err := equals(v, ov, op.as); err == nil {
				if eq {
					return true, nil
				}
			} else {
				return false, err
			}
		}
	}
	return false, nil
}
