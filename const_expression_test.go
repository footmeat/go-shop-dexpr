package dexpr

import "testing"

func TestConstExpression_Evaluate(t *testing.T) {
	type fields struct {
		value bool
	}
	type args struct {
		args []interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "returns TRUE when constant value is TRUE",
			fields: fields{
				value: true,
			},
			args:    args{},
			want:    true,
			wantErr: false,
		},
		{
			name: "returns FALSE when constant value is FALSE",
			fields: fields{
				value: true,
			},
			args:    args{},
			want:    true,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := &ConstExpression{
				value: tt.fields.value,
			}
			got, err := e.Evaluate(tt.args.args...)
			if (err != nil) != tt.wantErr {
				t.Errorf("ConstExpression.Evaluate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ConstExpression.Evaluate() = %v, want %v", got, tt.want)
			}
		})
	}
}
