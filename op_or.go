package dexpr

import "fmt"

// OpOr ... ... ... ... ... ... ...
type OpOr struct {
	operands []Expression
}

// Evaluate evaluates boolean OR operator.
// The function has an optional parameter, which is a field map.
func (op *OpOr) Evaluate(args ...interface{}) (bool, error) {
	if len(op.operands) == 0 {
		return false, fmt.Errorf("OpOr.Evaluate(): operand list is empty")
	}
	for _, t := range op.operands {
		if b, err := t.Evaluate(args...); err == nil {
			if b {
				return true, nil
			}
		} else {
			return false, err
		}
	}
	return false, nil
}
