package dexpr

import "testing"
import "fmt"

type errorExpression struct{}

func (e *errorExpression) Evaluate(args ...interface{}) (bool, error) {
	return false, fmt.Errorf("this error is produced for test purposes")
}

func TestExpression_NestedAndOrNor(t *testing.T) {
	tests := []struct {
		name    string
		expr    Expression
		want    bool
		wantErr bool
	}{
		{
			name: "(TRUE || FALSE) && TRUE should evaluate to TRUE",
			expr: &OpAnd{
				operands: []Expression{
					&OpOr{
						operands: []Expression{
							&trueExpression{}, &falseExpression{},
						},
					},
					&trueExpression{},
				},
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "(TRUE || FALSE) && FALSE should evaluate to FALSE",
			expr: &OpAnd{
				operands: []Expression{
					&OpOr{
						operands: []Expression{
							&trueExpression{}, &falseExpression{},
						},
					},
					&falseExpression{},
				},
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "(TRUE && FALSE) || TRUE should evaluate to TRUE",
			expr: &OpOr{
				operands: []Expression{
					&OpAnd{
						operands: []Expression{
							&trueExpression{}, &falseExpression{},
						},
					},
					&trueExpression{},
				},
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "(TRUE && FALSE) || FALSE should evaluate to FALSE",
			expr: &OpOr{
				operands: []Expression{
					&OpAnd{
						operands: []Expression{
							&trueExpression{}, &falseExpression{},
						},
					},
					&falseExpression{},
				},
			},
			want:    false,
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.expr.Evaluate()
			if (err != nil) != tt.wantErr {
				t.Errorf("expr.Evaluate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("expr.Evaluate() = %v, want %v", got, tt.want)
			}
		})
	}
}
