package dexpr

import (
	"encoding/json"
	"reflect"
	"testing"
)

func TestUnmarshal_buildExpression(t *testing.T) {
	type args struct {
		args []interface{}
	}
	tests := []struct {
		name        string
		jsonSrc     string
		expr        Expression
		evTrueArgs  []args
		evFalseArgs []args
	}{
		{
			name: `(TRUE or FALSE or not FALSE) and ('some_field' eq (string)"hello"`,
			jsonSrc: `{
				"$and": [
					{
						"$or": [
							true,
							false,
							{
								"$not": false
							}
						]
					},
					{
						"some_field": {
							"$eq": "hello",
							"$as": "string"
						}
					},
					{
						"$nor": [
							{
								"my_field_1": {
									"$gt": 100,
									"$as": "integer"
								}
							},
							{
								"my_field_2": {
									"$lt": 200,
									"$as": "integer"
								}
							},
							{
								"my_field_3": {
									"$neq": false,
									"$as": "boolean"
								}
							}
						]
					}
				]
			}`,
			expr: &OpAnd{
				operands: []Expression{
					&OpOr{
						operands: []Expression{
							&ConstExpression{value: true},
							&ConstExpression{value: false},
							&OpNot{
								operand: &ConstExpression{value: false},
							},
						},
					},
					&OpFieldEq{
						as:    valueTypeString,
						field: "some_field",
						value: "hello",
					},
					&OpNor{
						operands: []Expression{
							&OpFieldGT{
								as:    valueTypeInteger,
								field: "my_field_1",
								value: float64(100),
							},
							&OpFieldLT{
								as:    valueTypeInteger,
								field: "my_field_2",
								value: float64(200),
							},
							&OpFieldNeq{
								as:    valueTypeBoolean,
								field: "my_field_3",
								value: false,
							},
						},
					},
				},
			},
			evTrueArgs: []args{
				args{
					args: []interface{}{
						map[string]interface{}{
							"some_field": "hello",
							"my_field_1": float64(100), // 100 > 100 == false
							"my_field_2": float64(200), // 200 < 200 == false
							"my_field_3": false,        // false != false == false
						},
					},
				},
			},
			evFalseArgs: []args{
				args{
					args: []interface{}{
						map[string]interface{}{
							"some_field": "hello",
							"my_field_1": float64(90),  // 90 > 100 == false
							"my_field_2": float64(210), // 210 < 200 == false
							"my_field_3": true,         // true != false == true
						},
					},
				},
				args{
					args: []interface{}{
						map[string]interface{}{
							"some_field": "world",
						},
					},
				},
				args{
					args: []interface{}{
						map[string]interface{}{
							"another_field": "hello",
						},
					},
				},
			},
		},
		{
			name: "build field value-in operator",
			jsonSrc: `{
				"type": {
					"$in": ["sofa", "chair", "bed"],
					"$as": "string"
				}
			}`,
			expr: &OpFieldIn{
				as:    valueTypeString,
				field: "type",
				values: []interface{}{
					"sofa", "chair", "bed",
				},
			},
			evTrueArgs: []args{
				args{
					args: []interface{}{
						map[string]interface{}{
							"type": "sofa",
						},
					},
				},
			},
			evFalseArgs: []args{
				args{
					args: []interface{}{
						map[string]interface{}{
							"type": "door",
						},
					},
				},
			},
		},
		{
			name: "build field greater than operator",
			jsonSrc: `{
				"weight": {
					"$gt": 1000.0,
					"$as": "float"
				}
			}`,
			expr: &OpFieldGT{
				as:    valueTypeFloat,
				field: "weight",
				value: float64(1000),
			},
			evTrueArgs: []args{
				args{
					args: []interface{}{
						map[string]interface{}{
							"weight": float64(2000),
						},
					},
				},
			},
			evFalseArgs: []args{
				args{
					args: []interface{}{
						map[string]interface{}{
							"weight": float64(900),
						},
					},
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var data interface{}
			err := json.Unmarshal([]byte(tt.jsonSrc), &data)
			if err != nil {
				t.Fatal("incorrect JSON")
			}
			expr := buildExpression(data)
			if !reflect.DeepEqual(expr, tt.expr) {
				t.Errorf("buildExpression(): result does not match expected expression")
			}
			for _, args := range tt.evTrueArgs {
				res, err := expr.Evaluate(args.args...)
				if err != nil {
					t.Errorf("buildExpression(): error occurred while evaluating expression")
				}
				if !res {
					t.Errorf("buildExpression(): result evaluated to %v, want true", res)
				}
			}
			for _, args := range tt.evFalseArgs {
				res, err := expr.Evaluate(args.args...)
				if err != nil {
					t.Errorf("buildExpression(): error occurred while evaluating expression")
				}
				if res {
					t.Errorf("buildExpression(): result evaluated to %v, want false", res)
				}
			}
		})
	}
}

func TestUnmarshal_buildFieldExpression_Panic(t *testing.T) {
	tests := []struct {
		name    string
		jsonSrc string
	}{
		{
			name: "should panic when '$as' property not specified",
			jsonSrc: `{
				"some_field": {
					"$eq": "hello"
				}
			}`,
		},
		{
			name: "should panic when value type is unknown",
			jsonSrc: `{
				"some_field": {
					"$eq": "hello",
					"$as": "complex"
				}
			}`,
		},
		{
			name: "should panic when field operator is unknown",
			jsonSrc: `{
				"some_field": {
					"$xx": "hello",
					"$as": "integer"
				}
			}`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var name string
			defer func() {
				if r := recover(); r == nil {
					t.Errorf("expected panic: %v", name)
				}
			}()
			var data interface{}
			err := json.Unmarshal([]byte(tt.jsonSrc), &data)
			if err != nil {
				t.Fatal("incorrect JSON")
			}
			name = tt.name
			buildExpression(data)
		})
	}
}

func TestUnmarshal_buildExpression_Panic(t *testing.T) {
	tests := []struct {
		name      string
		arg       interface{}
		wantPanic bool
	}{
		{
			name:      "should panic when argument is neither (bool) nor (map[string]interface{})",
			arg:       nil,
			wantPanic: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			defer func() {
				r := recover()
				if (r == nil) && tt.wantPanic {
					t.Errorf("no expected panic occurred: %v", tt.name)
				}
				if (r != nil) && !tt.wantPanic {
					t.Errorf("unexpected panic occurred: %v", tt.name)
				}
			}()
			buildExpression(tt.arg)
		})
	}
}
