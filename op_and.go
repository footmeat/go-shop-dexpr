package dexpr

import (
	"fmt"
)

// OpAnd ...
type OpAnd struct {
	operands []Expression
}

// Evaluate evaluates boolean AND operator.
func (op *OpAnd) Evaluate(args ...interface{}) (bool, error) {
	if len(op.operands) == 0 {
		return false, fmt.Errorf("OpAnd.Evaluate(): operand list is empty")
	}
	for _, t := range op.operands {
		if b, err := t.Evaluate(args...); err == nil {
			if !b {
				return false, nil
			}
		} else {
			return false, err
		}
	}
	return true, nil
}
