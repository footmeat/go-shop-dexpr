package dexpr

import (
	"testing"
)

func TestOpOr_Evaluate(t *testing.T) {
	type fields struct {
		operands []Expression
	}
	type args struct {
		args []interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{
			name:    "returns true when at least one operand is true",
			fields:  fields{operands: []Expression{&falseExpression{}, &trueExpression{}}},
			args:    args{},
			want:    true,
			wantErr: false,
		},
		{
			name:    "returns false when all operands are false",
			fields:  fields{operands: []Expression{&falseExpression{}, &falseExpression{}}},
			args:    args{},
			want:    false,
			wantErr: false,
		},
		{
			name:    "should return error when operands is nil",
			fields:  fields{operands: nil},
			args:    args{},
			want:    false,
			wantErr: true,
		},
		{
			name:    "should return error when list of operands is empty",
			fields:  fields{operands: make([]Expression, 0)},
			args:    args{},
			want:    false,
			wantErr: true,
		},
		{
			name: "should return error, when after number of falsy operands one evaluates with error",
			// All operands that precede error-producing operand should evaluate to false.
			// Otherwise, the operator will quit evaluating its operands and will return true.
			fields:  fields{operands: []Expression{&falseExpression{}, &errorExpression{}}},
			args:    args{},
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			op := &OpOr{
				operands: tt.fields.operands,
			}
			got, err := op.Evaluate(tt.args.args...)
			if (err != nil) != tt.wantErr {
				t.Errorf("OpOr.Evaluate() error %v, wantErr = %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("OpOr.Evaluate() = %v, want %v", got, tt.want)
			}
		})
	}
}
